﻿using Evolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class Text : IHasFitness<Text>, IHasDna
    {
        public static string Pattern { get; } = @"
Ma to-to nazwe, ale nic poza tym.
Za piec dukatow nie wzialbym w dzierzawe
tego zagonu. I zreszta Norwegia
Czy Polska wiecej na nim nie zarobia
Gdyby go nawet od reki sprzedaly.".Replace("\r", "");//.Replace('\n', ' ').Replace("\r", "").Replace(' ', '_');
        private double[] dna;

        public double[] Dna
        {
            get => dna;
            set
            {
                this.recalculateFitness = true;
                this.dna = value;
            }
        }

        private bool recalculateFitness = true;
        private double fitness;
        public double Fitness
        {
            get
            {
                if (recalculateFitness)
                    this.fitness = Pattern.Zip(this.Dna, (c1, c2) => c1 == c2).Count(c => c);
                return this.fitness;
            }
        }

        public override string ToString()
        {
            return new string(this.Dna.Select(d => (char)d).ToArray());
        }
    }
}
