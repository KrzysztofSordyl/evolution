﻿using Evolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class TextRandomizer : IDnaRandomizer<Text>
    {
        private readonly Random random = new Random();

        public Text GetRandom()
        {
            return new Text()
            {
                Dna = Enumerable.Range(0, Text.Pattern.Length).Select(i => (double)this.random.Next('A', 'z' + 1)).ToArray()
            };
        }

        public IEnumerable<Text> GetRandom(int amount)
        {
            return Enumerable.Range(0, amount).Select(i => this.GetRandom());
        }

        public double[] GetRandomDna(double[] baseDna, double mutationChance)
        {
            return baseDna
                .Select(d =>
                {
                    if (this.random.NextDouble() < mutationChance)
                    {
                        int r = this.random.Next(' ', '~' + 2);
                        return r == '~' + 1 ? '\n' : r;
                    }
                    else return d;
                })
                .Cast<double>().ToArray();
        }
    }
}
