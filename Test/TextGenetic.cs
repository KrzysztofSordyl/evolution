﻿using Evolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class TextGenetic : GeneticAlgorithmBase<Text>
    {
        public TextGenetic(int size) : base(new TextRandomizer(), size)
        {
        }
    }
}
