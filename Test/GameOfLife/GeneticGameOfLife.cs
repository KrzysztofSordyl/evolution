﻿using Evolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.GameOfLife
{
    public class GeneticGameOfLife : GeneticAlgorithmBase<GameOfLife>
    {
        public GeneticGameOfLife(int size, int cellAreaSize, int cellAreaMargins) : base(new GameOfLifeRandomizer(cellAreaSize, cellAreaMargins), size)
        {

        }
    }
}
