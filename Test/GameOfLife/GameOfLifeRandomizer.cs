﻿using Evolution;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.GameOfLife
{
    public class GameOfLifeRandomizer : IDnaRandomizer<GameOfLife>
    {
        private readonly Random random = new Random();

        public GameOfLifeRandomizer(int size, int margins)
        {
            this.size = size;
            this.margins = margins;
        }

        private int size;
        private int margins;

        public GameOfLife GetRandom()
        {
            var points = new List<Point>();
            for (int y = 0; y < this.size + 2 * this.margins; y++)
            {
                for (int x = 0; x < this.size + 2 * this.margins; x++)
                {
                    if (y >= this.margins && y < this.size + this.margins &&
                        x >= this.margins && x < this.size + this.margins &&
                        this.random.NextDouble() < 0.5)
                        points.Add(new Point(x, y));
                }
            }
            return new GameOfLife(this.size + 2 * this.margins, points);
        }

        public IEnumerable<GameOfLife> GetRandom(int amount)
        {
            return Enumerable.Range(0, amount).Select(i => this.GetRandom());
        }

        public double[] GetRandomDna(double[] baseDna, double mutationChance)
        {
            var board = new bool[this.size + 2 * this.margins, this.size + 2 * this.margins];
            for (int y = 0; y < this.size + 2 * this.margins; y++)
            {
                for (int x = 0; x < this.size + 2 * this.margins; x++)
                {
                    if (y >= this.margins && y < this.size + this.margins &&
                        x >= this.margins && x < this.size + this.margins)
                        board[y, x] = 
                            (this.random.NextDouble() < mutationChance ^ baseDna[y * (this.size + 2 * this.margins) + x] == 0);
                }
            }
            return board.Cast<bool>()
                .Select(c => c ? 1.0 : 0.0)
                .ToArray();
        }
    }
}
