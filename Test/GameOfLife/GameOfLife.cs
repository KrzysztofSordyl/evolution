﻿using Evolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.GameOfLife
{
    public class GameOfLife : IHasFitness<GameOfLife>, IHasDna
    {
        public GameOfLife() : this(20, new List<Point>()) { }

        public GameOfLife(int size, IEnumerable<Point> cells)
        {
            this.Board = new bool[size, size];
            this.StartBoard = new bool[size, size];
            foreach (var cell in cells)
            {
                this.Board[cell.Y, cell.X] = true;
                this.StartBoard[cell.Y, cell.X] = true;
            }
        }

        public bool[,] StartBoard { get; set; }
        private bool[,] _board;
        public bool[,] Board
        {
            get => this._board;
            private set
            {
                this._board = value;
                this.Size = new Point(this.Board.GetLength(1), this.Board.GetLength(0));
            }
        }
        public Point Size { get; private set; }

        public double? fitness = null;
        public double Fitness
        {
            get
            {
                if (!this.fitness.HasValue)
                {
                    bool[,] previous = this.Board;
                    bool[,] current = null;
                    int changes = 0;
                    this.Run(5, (b) =>
                    {
                        current = b;
                        for (int y = 0; y < this.Size.Y; y++)
                        {
                            for (int x = 0; x < this.Size.X; x++)
                            {
                                if (previous[y, x] != current[y, x])
                                    changes++;
                            }
                        }
                        previous = current;
                    });
                    if (this.Board.Cast<bool>().Sum(c => c ? 1 : 0) == 0)
                        this.fitness = 0;
                    else if (changes == 0)
                        this.fitness = this.Board.Cast<bool>().Sum(c => c ? 1 : 0);
                    else
                        this.fitness = this.Board.Cast<bool>().Sum(c => c ? 1 : 0) / (double)changes;
                }
                return this.fitness.Value;
            }
        }

        public double[] Dna
        {
            get => this.StartBoard
                .Cast<bool>()
                .Select(b => b ? 1.0 : 0.0)
                .ToArray();
            set => value
                .Select((d, i) => new { d, i })
                .ToList()
                .ForEach(d =>
                {
                    this.Board.SetValue(d.d == 0 ? false : true, d.i / 20, d.i % 20);
                    this.StartBoard.SetValue(d.d == 0 ? false : true, d.i / 20, d.i % 20);
                });
        }

        public void Run(int generations, Action<bool[,]> callback)
        {
            for (int g = 0; g < generations; g++)
            {
                this.Run();
                callback(this.Board);
            }
        }

        public void Run()
        {
            bool[,] nextBoard = new bool[this.Size.Y, this.Size.X];

            for (int y = 0; y < this.Size.Y; y++)
            {
                for (int x = 0; x < this.Size.X; x++)
                {
                    nextBoard[y, x] = this.checkCell(new Point(x, y), this.Board[x, y]);
                }
            }

            this.Board = nextBoard;
        }

        private bool checkCell(Point cell, bool alive)
        {
            int neighbors = 0;
            for (int h = cell.Y == 0 ? 0 : -1; h <= (cell.Y == this.Size.Y - 1 ? 0 : 1); h++)
            {
                for (int w = cell.X == 0 ? 0 : -1; w <= (cell.X == this.Size.X - 1 ? 0 : 1); w++)
                {
                    if (this.Board[cell.Y + h, cell.X + w])
                        neighbors++;
                }
            }
            if (alive && (neighbors == 2 || neighbors == 3))
                return true;
            if (!alive && neighbors == 3)
                return true;
            return false;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 0; y < this.Size.Y; y++)
            {
                for (int x = 0; x < this.Size.X; x++)
                {
                    sb.Append(this.StartBoard[y, x] ? "X" : ".");
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}
