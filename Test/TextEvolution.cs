﻿using Evolution;
using Evolution.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public class TextEvolution : EvolutionaryAlgorithmBase<Text>
    {
        private readonly Random random = new Random();

        public TextEvolution(int populationSize) : base(new Population<Text>(new TextRandomizer(), populationSize))
        {

        }

        protected override void mutate(Text member)
        {
            member.Dna = member.Dna
                .Select(d => random.NextDouble() <= 0.05 ? (double)this.random.Next('A', 'z' + 1) : d)
                .ToArray();
        }

        protected override IEnumerable<Text> populate(int count)
        {
            List<Text> children = new List<Text>();
            while (children.Count != count)
            {
                Text t1 = null;
                foreach (var item in this.Population.Sample((count - children.Count) * 2, t => t.Fitness, true))
                {
                    if (t1 == null)
                        t1 = item;
                    else
                    {
                        if (t1 != item)
                            children.Add(new Text()
                            {
                                Dna = this.combineTexts(
                                    new string(t1.Dna.Select(d => (char)d).ToArray()),
                                    new string(item.Dna.Select(d => (char)d).ToArray())
                                    ).Select(c => (double)c).ToArray()
                            });
                        t1 = null;
                    }
                }
            }
            return children;
            #region old
            //return Enumerable.Range(0, count).Select(x =>
            //{
            //    int i = this.random.Next(this.Population.Count);
            //    int j;
            //    do
            //    {
            //        j = this.random.Next(this.Population.Count);
            //    } while (i == j);
            //    return new Text()
            //    {
            //        Value = new string(this.Population.ElementAt(i).Value
            //        .Zip(
            //            this.Population.ElementAt(j).Value,
            //            (c1, c2) => this.random.NextDouble() < 0.5 ? c1 : c2)
            //        .ToArray())
            //    };
            //}).ToList();
            #endregion
        }
        private string combineTexts(string t1, string t2)
        {
            return new string(t1.Zip(t2,
                        (c1, c2) => this.random.NextDouble() < 0.5 ? c1 : c2)
                    .ToArray());
        }
    }
}
