﻿using Evolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.GameOfLife;

namespace Test
{
    class Program
    {
        public static void Main(string[] args)
        {
            textGenetic();
            //immutableGenetic();
            Console.ReadLine();
        }

        private static void textGenetic()
        {
            int previousMaxFitness = 0;
            TextGenetic te = new TextGenetic(400);
            int maxFitness = (int)te.Population.MaxFitness;
            while (maxFitness != Text.Pattern.Length)
            {
                Console.Write("X");
                if (previousMaxFitness < maxFitness)
                {
                    Console.WriteLine("\nGeneration: " + te.Generation);
                    Console.WriteLine("MaxFitness: " + maxFitness);
                    Console.WriteLine("Best:\n" + te.Population.First(t => t.Fitness == maxFitness));
                    previousMaxFitness = maxFitness;
                }
                double minFitness = te.Population.AverageFitness;
                if (Math.Abs(minFitness - maxFitness) < 1)
                {
                    te.MutationChance = 0.03;
                    //Console.WriteLine("Killed 390");
                    te.Evolve(KillingStrategy.FixedAmount | KillingStrategy.MinimalFittness, 390, minFitness);
                }
                else
                {
                    te.MutationChance = 0.01;
                    te.Evolve(KillingStrategy.Random | KillingStrategy.MinimalFittness, 300, minFitness);
                }
                maxFitness = (int)te.Population.MaxFitness;
            }
            Console.WriteLine("Generation: " + te.Generation);
            Console.WriteLine("MinFitness: " + te.Population.MinFitness);
            Console.WriteLine("AverageFitness: " + te.Population.AverageFitness);
            Console.WriteLine("First: " + te.Population.First(t => t.Fitness == te.Population.MaxFitness));
        }

        private static void immutableGenetic()
        {
            double previousMaxFitness = 0;
            GeneticGameOfLife ggol = new GeneticGameOfLife(200, 12, 4);
            double maxFitness = ggol.Population.MaxFitness;
            while (maxFitness < 1)
            {
                Console.Write("X");
                if (previousMaxFitness < maxFitness)
                {
                    Console.WriteLine("\nGeneration: " + ggol.Generation);
                    Console.WriteLine("MaxFitness: " + maxFitness);
                    Console.WriteLine("Best:\n" + ggol.Population.First(t => t.Fitness == maxFitness));
                    previousMaxFitness = maxFitness;
                }
                double minFitness = (ggol.Population.AverageFitness + maxFitness) / 2;
                if (Math.Abs(minFitness - maxFitness) < 0.0005)
                {
                    ggol.MutationChance = 0.03;
                    ggol.Evolve(KillingStrategy.FixedAmount | KillingStrategy.MinimalFittness, 190, minFitness);
                }
                else
                {
                    ggol.MutationChance = 0.01;
                    ggol.Evolve(KillingStrategy.Random | KillingStrategy.MinimalFittness, 160, minFitness);
                }
                maxFitness = ggol.Population.MaxFitness;
            }
            Console.WriteLine("Generation: " + ggol.Generation);
            Console.WriteLine("MinFitness: " + ggol.Population.MinFitness);
            Console.WriteLine("AverageFitness: " + ggol.Population.AverageFitness);
            Console.WriteLine("First: " +
                new string(ggol.Population.First(t => t.Fitness == ggol.Population.MaxFitness).Dna.Select(d => (char)d).ToArray()));
        }
    }
}
