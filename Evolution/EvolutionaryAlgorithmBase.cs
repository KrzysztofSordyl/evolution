﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution
{
    public abstract class EvolutionaryAlgorithmBase<TItem> where TItem : IHasFitness<TItem>
    {
        protected EvolutionaryAlgorithmBase(Population<TItem> population)
        {
            Population = population ?? throw new ArgumentNullException(nameof(population));
        }
        protected EvolutionaryAlgorithmBase(IRandomizer<TItem> randomizer, int size)
            : this(new Population<TItem>(randomizer ?? throw new ArgumentNullException(nameof(randomizer)), size))
        {

        }

        public int Generation { get; private set; }

        public Population<TItem> Population { get; }

        public void Evolve(KillingStrategy strategy, int killedAmount, double minFitness)
        {
            int count = this.Population.Kill(strategy, killedAmount, minFitness);
            evolve(count);
        }

        public void Evolve(int killedAmount)
        {
            this.Population.Kill(KillingStrategy.FixedAmount | KillingStrategy.Random, killedAmount);
            evolve(killedAmount);
        }

        public void Evolve(double minFitness)
        {
            int killedAmount = this.Population.Kill(KillingStrategy.MinimalFittness, 0, minFitness);
            evolve(killedAmount);
        }
        
        private void evolve(int killedAmount)
        {
            List<TItem> children = this.populate(killedAmount).ToList();
            children.ForEach(c => this.mutate(c));
            this.Population.Fill(children);
            this.Generation++;
        }

        protected abstract IEnumerable<TItem> populate(int count);

        protected abstract void mutate(TItem member);
    }
}
