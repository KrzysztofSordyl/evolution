﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution
{
    public interface IDnaRandomizer<T> : IRandomizer<T>
    {
        double[] GetRandomDna(double[] baseDna, double mutationChance);
    }
}
