﻿using Evolution.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution
{
    public class Population<TItem> : IEnumerable<TItem> where TItem : IHasFitness<TItem>
    {
        private readonly List<TItem> members = new List<TItem>();

        public Population(IEnumerable<TItem> members)
        {
            this.members.AddRange(members);
        }

        public Population(IRandomizer<TItem> randomizer, int size = 100) : this(randomizer.GetRandom(size))
        {

        }

        public int Count => this.members.Count();

        public double MinFitness => this.members.Min(m => m.Fitness);

        public double MaxFitness => this.members.Max(m => m.Fitness);

        public double AverageFitness => this.members.Average(m => m.Fitness);

        public int Kill(KillingStrategy strategy, int count, double minFitness = 0)
        {
            if (count > this.Count)
                throw new InvalidOperationException("Can not kill more members than the population counts.");

            if (strategy.HasFlag(KillingStrategy.MinimalFittness))
            {
                if (strategy.HasFlag(KillingStrategy.FixedAmount))
                {
                    if (this.members.Count(m => m.Fitness < minFitness) > count)
                    {
                        if (strategy.HasFlag(KillingStrategy.Random))
                        {
                            List<TItem> toRemove = this.members
                                .Where(m => m.Fitness < minFitness)
                                .Sample(count, m => m.Fitness)
                                .ToList();
                            return this.members.RemoveAll(m => toRemove.Contains(m));
                        }
                        else
                        {
                            List<TItem> toRemove = this.members
                                .Where(m => m.Fitness < minFitness)
                                .OrderBy(m => m.Fitness)
                                .Take(count)
                                .ToList();
                            return this.members.RemoveAll(m => toRemove.Contains(m));
                        }
                    }
                    else
                    {
                        int removed = this.members.RemoveAll(m => m.Fitness < minFitness);
                        if (removed != count)
                        {
                            int toRemoveCount = count - removed;
                            if (strategy.HasFlag(KillingStrategy.Random))
                            {
                                List<TItem> toRemove = this.members
                                    .Sample(toRemoveCount, m => m.Fitness)
                                    .ToList();
                                return removed + this.members.RemoveAll(m => toRemove.Contains(m));
                            }
                            else
                            {
                                List<TItem> toRemove = this.members
                                    .OrderBy(m => m.Fitness)
                                    .Take(toRemoveCount)
                                    .ToList();
                                return removed + this.members.RemoveAll(m => toRemove.Contains(m));
                            }
                        }
                        else
                            return removed;
                    }
                }
                else
                {
                    List<TItem> toRemove = this.members
                        .Where(m => m.Fitness < minFitness)
                        .OrderBy(m => m.Fitness)
                        .Take(count)
                        .ToList();
                    return this.members.RemoveAll(m => toRemove.Contains(m));
                }
            }
            else
            {
                if (strategy.HasFlag(KillingStrategy.Random))
                {
                    List<TItem> toRemove = this.members
                        .Sample(count, m => m.Fitness)
                        .ToList();
                    return this.members.RemoveAll(m => toRemove.Contains(m));
                }
                else
                {
                    List<TItem> toRemove = this.members
                        .OrderBy(m => m.Fitness)
                        .Take(count)
                        .ToList();
                    return this.members.RemoveAll(m => toRemove.Contains(m));
                }
            }
        }

        public void Fill(IEnumerable<TItem> members)
        {
            this.members.AddRange(members);
        }

        public IEnumerator<TItem> GetEnumerator()
        {
            return this.members.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    [Flags]
    public enum KillingStrategy
    {
        None = 0,
        Random = 0x1,
        MinimalFittness = 0x2,
        FixedAmount = 0x4
    }
}
