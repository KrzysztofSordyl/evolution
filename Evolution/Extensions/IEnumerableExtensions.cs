﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Sample<T>(this IEnumerable<T> source, int amount, bool returning = false)
        {
            Random random = new Random();
            if (returning)
            {
                for (int i = 0; i < amount; i++)
                {
                    yield return source.ElementAt(random.Next(source.Count()));
                }
            }
            else
            {
                if (amount > source.Count())
                    throw new InvalidOperationException("Can not draw amount of items bigger than collection size while returning is switched off.");
                List<T> toDraw = new List<T>(source);
                for (int i = 0; i < amount; i++)
                {
                    int index = random.Next(toDraw.Count);
                    yield return toDraw.ElementAt(index);
                    toDraw.RemoveAt(index);
                }
            }
        }
        public static IEnumerable<T> Sample<T>(this IEnumerable<T> source, int amount, Func<T, double> scoringFunction, bool returning = false)
        {
            double cumulatedRates = 0;
            var toDraw = source
                .Select(k => new RatedItem<RatedItem<T>>(new RatedItem<T>(k, scoringFunction(k)), 0))
                .ToList();
            toDraw.ForEach(ri =>
                {
                    cumulatedRates += ri.Item.Rate;
                    ri.Rate = cumulatedRates;
                });
            Random random = new Random();

            if (returning)
            {
                for (int i = 0; i < amount; i++)
                {
                    double randomValue = random.NextDouble() * cumulatedRates;
                    var randomized = toDraw.First(ri => ri.Rate >= randomValue).Item;
                    
                    yield return randomized.Item;
                }
            }
            else
            {
                if (amount > source.Count())
                    throw new InvalidOperationException("Can not draw amount of items bigger than collection size while returning is switched off.");

                toDraw.Reverse();
                for (int i = 0; i < amount; i++)
                {
                    double randomValue = random.NextDouble() * cumulatedRates;
                    var searched = toDraw
                        .TakeWhile(ri => ri.Rate >= randomValue)
                        .ToList();
                    var found = searched.Last();
                    searched.Remove(found);
                    toDraw.Remove(found);
                    cumulatedRates -= found.Item.Rate;
                    searched.ForEach(ri =>
                    {
                        ri.Rate -= found.Item.Rate;
                    });
                    yield return found.Item.Item;
                }
            }
        }
    }

    public class RatedItem<T>
    {
        public RatedItem(T item, double rate)
        {
            this.Item = item;
            this.Rate = rate;
        }

        public T Item { get; set; }

        public double Rate { get; set; }
    }
}
