﻿using Evolution.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evolution
{
    public abstract class GeneticAlgorithmBase<TItem> : EvolutionaryAlgorithmBase<TItem> where TItem : class, IHasFitness<TItem>, IHasDna, new()
    {
        private Random random = new Random();

        protected GeneticAlgorithmBase(Population<TItem> population) : base(population)
        {

        }

        protected GeneticAlgorithmBase(IDnaRandomizer<TItem> randomizer, int size) : base(randomizer, size)
        {
            this.randomizer = randomizer;
        }

        private readonly IDnaRandomizer<TItem> randomizer;
        private double mutationChance;
        public double MutationChance
        {
            get => this.mutationChance;
            set
            {
                if (value > 1 || value < 0)
                    throw new InvalidOperationException("Chance must be between 0 and 1");
                this.mutationChance = value;
            }
        }

        protected override void mutate(TItem member)
        {
            if (this.randomizer == null)
                member.Dna = member.Dna.Select(c =>
                {
                    if (random.NextDouble() <= this.MutationChance)
                        return (char)this.random.NextDouble();
                    else
                        return c;
                }).ToArray();
            else
                member.Dna = this.randomizer.GetRandomDna(member.Dna, this.MutationChance);
        }

        protected override IEnumerable<TItem> populate(int count)
        {
            List<TItem> children = new List<TItem>();
            while (children.Count != count)
            {
                TItem m1 = null;
                foreach (var item in this.Population.Sample((count - children.Count) * 2, m => m.Fitness, true))
                {
                    if (m1 == null)
                        m1 = item;
                    else
                    {
                        if (m1 != item)
                            children.Add(new TItem()
                            {
                                Dna = this.combineDna(m1.Dna, item.Dna)
                            });
                        m1 = null;
                    }
                }
            }
            return children;
        }
        private double[] combineDna(double[] d1, double[] d2)
        {
            return d1.Zip(d2,
                        (c1, c2) => this.random.NextDouble() < 0.5 ? c1 : c2)
                    .ToArray();
        }
    }
}
